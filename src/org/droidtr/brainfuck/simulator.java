package org.droidtr.brainfuck;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class simulator extends Activity {
    LinearLayout.LayoutParams yuva=null;
    TextView source=null;
    TextView out=null;
    EditText in=null;
    int current=0;
    char array[]=new char[102400];
    int index=0;
    char register=0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout main = new LinearLayout(this);
        setContentView(main);
        main.setBackgroundColor(Color.RED);
        main.setOrientation(LinearLayout.VERTICAL);

        yuva = new LinearLayout.LayoutParams(-1, -2);
        yuva.weight = 1;

        LinearLayout stdout = new LinearLayout(this);
        LinearLayout stdin = new LinearLayout(this);
        LinearLayout code = new LinearLayout(this);
        LinearLayout buts = new LinearLayout(this);
        LinearLayout buts2 = new LinearLayout(this);

        source = new TextView(this);
        source.setSingleLine(false);
        source.setTextSize(source.getTextSize());
        source.setText("++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.");
        out = new TextView(this);
        out.setSingleLine(false);
        out.setTextSize(out.getTextSize());
        out.setBackgroundColor(Color.TRANSPARENT);
        out.setTextColor(Color.WHITE);
        in = new EditText(this);
        in.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        in.setBackgroundColor(Color.TRANSPARENT);
        in.setTextColor(Color.WHITE);
        in.setTextSize(in.getTextSize());


        stdout.setLayoutParams(yuva);
        stdin.setLayoutParams(yuva);
        code.setLayoutParams(yuva);
        buts.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        buts2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));

        stdout.setBackgroundColor(Color.rgb(23, 17, 12));
        stdin.setBackgroundColor(Color.rgb(26, 18, 23));
        code.setBackgroundColor(Color.rgb(16, 27, 25));
        buts.setBackgroundColor(Color.BLACK);
        buts2.setBackgroundColor(Color.BLACK);

        code.addView(source);
        stdin.addView(in);
        stdout.addView(out);

        main.addView(stdout);
        main.addView(stdin);
        main.addView(code);
        main.addView(buts);
        main.addView(buts2);


        buts.addView(getbutton("<",Color.BLACK));
        buts.addView(getbutton(">",Color.BLACK));
        buts.addView(getbutton(".",Color.BLACK));
        buts.addView(getbutton(",",Color.BLACK));
        buts.addView(getbutton("^",Color.DKGRAY));
        buts.addView(getbutton("*",Color.DKGRAY));
        buts.addView(getbutton("/",Color.DKGRAY));
        buts.addView(getbutton("0",Color.DKGRAY));
        buts.addView(getbutton("O",Color.DKGRAY));
        buts.addView(getbutton("+",Color.BLACK));
        buts.addView(getbutton("-",Color.BLACK));
        buts.addView(getbutton("[",Color.BLACK));
        buts.addView(getbutton("]",Color.BLACK));
        buts2.addView(getbutton("run",Color.BLACK));
        buts2.addView(getbutton("!",Color.GRAY));
        buts2.addView(getbutton("$",Color.GRAY));
        buts2.addView(getbutton("~",Color.GRAY));
        buts2.addView(getbutton("(",Color.GRAY));
        buts2.addView(getbutton(")",Color.GRAY));
        buts2.addView(getbutton("del",Color.BLACK));
    }

    public Button getbutton(final String txt,int color){
        Button b = new Button(this);
        b.setText(txt);
        b.setLayoutParams(yuva);
        b.setTextColor(Color.WHITE);
        b.setBackgroundColor(color);
        b.setTextSize(25);
        b.setPadding(5,5,5,5);
        if(txt=="del") {
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(source.getText().toString().length()>=1) {
                        source.setText(source.getText().toString().substring(0, source.getText().toString().length() - 1));
                    }
                }
            });
            b.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    source.setText("");
                    in.setText("");
                    out.setText("");
                    return false;
                }
            });
        }else if(txt == "run"){
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(source.getText().toString().length()>=1) {
                        run();
                    }
                }
            });
        }else{
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    source.setText(source.getText().toString() + txt);
                }
            });
        }
        return b;
    }
    public void run(){
        array=new char[102400];
        index=0;
        current=0;
        try {
            out.setText(interpret(source.getText().toString()));
        }catch (Exception e){
            out.setText(e.toString());
        }
        
    }
    private String interpret(String s)
    {
        int c = 0;
        String output="";
        // Parsing through each character of the code 
        for (int i = 0; i != s.length() ; i++)
        {
            // BrainFuck is a tiny language with only 
            // eight instructions. In this loop we check   
            // and execute all those eight instructions 

            if(current>=in.getText().toString().split("").length){
                break;
            }
            // > moves the pointer to the right 
            if (s.charAt(i) == '>')
            {
                if (index == array.length - 1)//If array is full 
                    index = 0;//pointer is returned to zero 
                else
                    index ++;
            }
            if (s.charAt(i) == '~') {
                break;
            }

            if (s.charAt(i) == '!') {
                array[index]=register;
            }
            if(s.charAt(i) == '$'){
                register=array[index];
            }
                // < moves the pointer to the left
            else if (s.charAt(i) == '<')
            {
                if (index == 0) // If the pointer reaches zero 

                    // pointer is returned to rightmost array 
                    // position 
                    index = array.length - 1;
                else
                    index --;
            }

            // + increments the value of the array  
            // cell under the pointer 
            else if (s.charAt(i) == '+')
                array[index] ++;

            else if (s.charAt(i) == '*')
                array[index]= (char) (array[index]*2);

            else if (s.charAt(i) == '/')
                array[index]= (char) (array[index]/2);

            else if (s.charAt(i) == '$')
                array[index]= (char) (array[index]*register);


            else if (s.charAt(i) == '0')
                array[index]= 0;

            else if (s.charAt(i) == 'O')
                register=0;

                // - decrements the value of the array cell  
                // under the pointer 
            else if (s.charAt(i) == '-')
                array[index] --;

                // . outputs the character signified by the 
                // cell at the pointer 
            else if (s.charAt(i) == '.')
                output=output+(char)(array[index]);

                // , inputs a character and store it in the 
                // cell at the pointer 
            else if (s.charAt(i) == ',') {
                array[index] = in.getText().toString().toCharArray()[current];
                current++;
            }


            // [ jumps past the matching ] if the cell
            // under the pointer is 0
            else if (s.charAt(i) == '[')
            {
                if (array[index] == 0)
                {
                    i++;
                    while (c > 0 || s.charAt(i) != ']')
                    {
                        if (s.charAt(i) == '[')
                            c++;
                        else if (s.charAt(i) == ']')
                            c--;
                        i ++;
                    }
                }
            }
            // [ jumps past the matching ) if the cell
            // under the pointer is not 0
            else if (s.charAt(i) == '(')
            {
                if (array[index] != 0)
                {
                    i++;
                    while (c > 0 || s.charAt(i) != ')')
                    {
                        if (s.charAt(i) == '(')
                            c++;
                        else if (s.charAt(i) == ')')
                            c--;
                        i ++;
                    }
                }
            }

            // ] jumps back to the matching [ if the 
            // cell under the pointer is nonzero 
            else if (s.charAt(i) == ']')
            {
                if (array[index] != 0)
                {
                    i --;
                    while (c > 0 || s.charAt(i) != '[')
                    {
                        if (s.charAt(i) == ']')
                            c ++;
                        else if (s.charAt(i) == '[')
                            c --;
                        i --;
                    }
                    i --;
                }
            }
            // ] jumps back to the matching ( if the
            // cell under the pointer is zero
            else if (s.charAt(i) == ')')
            {
                if (array[index] == 0)
                {
                    i --;
                    while (c > 0 || s.charAt(i) != '(')
                    {
                        if (s.charAt(i) == ')')
                            c ++;
                        else if (s.charAt(i) == '(')
                            c --;
                        i --;
                    }
                    i --;
                }
            }
        }
        return output;
    }

}
